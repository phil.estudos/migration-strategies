# Run doc

## Docker
```sh
docker build . -t migration-strategies:${VERSION}
```
```sh
docker run -p 8000:8000 migration-strategies:${VERSION}
```

## Make
```sh
make all
```

Apos executar uma das opções basta acessar http://localhost:8000