# Os 6 R's: Estratégias de migração

<p align="center">
  <img src="./images/6rs.png">
</p>

## O que são estratégias de migração?

Estratégia de migração geralmente se refere ao processo de migração de todo o ambiente de aplicativos e sua infraestrutura de computação. Isso geralmente é impulsionado por decisões de negócios para otimização de custos, busca de mais agilidade, ou simplesmente pela atualização de sistemas antigos. Aqui estão alguns exemplos básicos de migração geral de aplicativos:

- Migrar um aplicativo de um data center local para outro
- De uma nuvem pública para um ambiente de nuvem privada ou vice-versa
- De um servidor local a um ambiente de nuvem pública
- Migrando um aplicativo de um provedor de nuvem para outro

## Entendendo os 6 R's

<p align="center">
  <img src="./images/entendendo_6rs.png">
</p>

### Retire
Se você estiver trabalhando em um ambiente com toneladas de aplicativos ou sistemas legados que parecem durar para sempre, a estratégia de retirada (aposentadoria) geralmente é implementada para ajudar a empacotar e reduzir os ativos a serem migrados. Ao remover os aplicativos que não estão em uso, você pode direcionar sua atenção para a migração e manutenção dos recursos que importam.

#### Quando utilizar essa estratégia?

- Um aplicativo é redundante ou obsoleto
- Um aplicativo legado não é compatível com a nuvem e não fornece mais valor produtivo
- Você decide refatorar ou recomprar um aplicativo

### Retain
Essa estratégia opta por "deixar as coisas como estão". Existem diversos cenários onde essa opção é adotada como por exemplo em um cenário de complience onde não é possível migrar a aplicação, mas o principal motivo para optar por essa estratégia é que a migração deve agregar algum valor para o negócio caso contrario a migração deve ser repensada.

#### Quando utilizar essa estratégia?
- Você adota um modelo de nuvem híbrida durante a migração
- Você está fortemente investido em aplicativos on-prem
- Um aplicativo legado não é compatível com a nuvem e funciona bem on-prem
- Você decide revisitar um aplicativo mais tarde

### Rehost (lift and shift)
Essa estratégia envolve mover aplicativos do ambiente local para a nuvem sem modificação. É comumente usado para migrar aplicativos legados em grande escala para atender a objetivos de negócios específicos, como um cronograma de lançamento de produto acelerado.A estratégia Rehost parece simples, e é. Existem ferramentas maduras (por exemplo, AWS VM Import/Export, Racemi, Sureline, C3DNA, River Meadow, AppOrbit, Corent etc.) que permitem que façamos isso com pouco esforço. A desvantagem é que, sem alterações na solução, ela tem capacidade limitada de aproveitar o ambiente nativo da nuvem.

#### Quando utilizar essa estratégia?
- Novo na nuvem
- Migrando aplicativos prontos para uso
- Migrando com prazo

#### Prós
- Velocidade de migração
- Risco reduzido de migração
- Pode ser automatizado ou assistido por ferramentas.
- Migração de sistemas inteiros (bancos de dados, VMs) com configuração

#### Contras
- Você não pode usar os serviços da cloud que oferecem vantagens operacionais, como bancos de dados gerenciados.
- Potencialmente herdar problemas de desempenho ou outros problemas

#### Case
O Cordant Group, uma empresa líder em gestão de valores mobiliários e instalações no Reino Unido, tinha uma extensa infraestrutura de TI para atender a uma ampla escala de operações comerciais. Poderia economizar de 40 a 50% nos custos ao passar do modelo de CapEx local para o modelo de OpEx baseado em nuvem. Ele usou a abordagem 'lift and shift' para migrar com sucesso todas as suas operações de TI para a nuvem AWS, incluindo vários sites, serviços de desktop do cliente e bancos de dados SQL.

### Replatform (lift, tinker and shift)
Essa estratégia de 'lift, tinker and shift' é uma versão modificada de rehosting. O Replatform permite que você faça algumas alterações de configuração nos aplicativos para melhor se adequar ao ambiente de nuvem sem alterar sua arquitetura principal. Os desenvolvedores geralmente aplicam essa abordagem para alterar a maneira como os aplicativos interagem com o banco de dados para que possam ser executados em plataformas gerenciadas como Google CloudSQL ou Amazon RDS.

#### Quando utilizar essa estratégia?
- Migrar com uma crise de tempo
- Aproveite os benefícios da nuvem sem refatorar o aplicativo
- Migrar um aplicativo local complexo com pequenos ajustes para obter benefícios na nuvem

#### Prós
- Opção de usar serviços próximos à infraestrutura existente sem exigir alteração de código
- Sem dependência de SO, hipervisor ou plataforma de hardware físico subjacente
- Oportunidade de utilizar recursos/funcionalidades modernas de OS/DB
- Em alguns casos, é possível usar ferramentas automatizadas 

#### Contras
- Requer planejamento e coordenação adicionais e esses projetos podem ser demorados e mais caros
- Conhecimento necessário desses "novos" serviços em nuvem

#### Case
O Pinterest migrou da nuvem herdada da AWS para o sistema de computação em nuvem de última geração quando alcançou mais de 250 milhões de clientes e atendeu a mais de 1.000 microsserviços com diferentes camadas de infraestrutura e ferramentas. Ele seguiu a abordagem 'levantar, mexer e mudar' para mover os microsserviços para contêineres do Docker com tecnologia Kubernetes. Como resultado, reduziu as horas de instância para os engenheiros e a tornou econômica.

### Repurchase
Na estratégia de recompra, você está fazendo a seleção para manobrar para um produto especial ou modelo de licenciamento. Um exemplo dessa estratégia pode ser usar a migração como uma chance de atualizar para uma versão mais moderna de um produto ou até mesmo passar de uma licença de outdoor para uma licença corporativa, ou vice-versa. A distinção importante é que você simplesmente não está alterando fundamentalmente o design do dispositivo ou sistema.

#### Quando utilizar essa estratégia?
- Você está substituindo o software por funções padrão como finanças, contabilidade, CRM, HRM, ERP, e-mail, CMS, etc.
- Um aplicativo legado não é compatível com a nuvem

#### Prós
- Reduz o esforço/aumenta a velocidade de migração
- Substitua sistemas legados por aplicativos modernos e SaaS

#### Contras
- Dificuldade com a compreensão das dependências em seus sistemas atuais
- Sua equipe precisa aprender a administrar um novo sistema em nuvem

#### Case
O Airbnb mudou para o Amazon RDS e abandonou o MySQL durante sua migração para a AWS. Procedimentos complexos, como replicação e dimensionamento, eram difíceis de executar com o MySQL. O Amazon RDS simplificou e tratou de muitas das tarefas administrativas demoradas associadas a bancos de dados. Como resultado, os engenheiros poderiam gastar mais tempo desenvolvendo. E todo o banco de dados foi migrado para o Amazon RDS com apenas 15 minutos de inatividade.

### Refactor/Rearchitect
Para a estratégia de refatoração ou rearquitetura, você normalmente é impulsionado por uma empresa robusta que precisa adicionar recursos, escala ou desempenho que serão difíceis de alcançar no ambiente existente. Embora essa estratégia às vezes possa ser a mais cara no cálculo das horas gastas, muitas vezes pode produzir os resultados mais eficazes, pois permite aproveitar ao máximo as vantagens que a nuvem oferece.

#### Quando utilizar essa estratégia?
- O aplicativo ganhará mais com a nuvem
- Há um forte impulso de negócios para adicionar escalabilidade, velocidade, desempenho
- Um aplicativo on-prem não é compatível com a nuvem

#### Prós
- Altamente maior eficiência, agilidade e custo aprimorado
- Eliminando a dependência de hardware personalizado ou plataformas de tecnologia proprietárias
- Sem infraestrutura/servidores para manter

#### Contras
- É necessária uma avaliação cuidadosa dos parceiros/fornecedores
- A rearquitetura geralmente cresce com tempo e esforço
- Requer uma compreensão muito boa de todos os aspectos do aplicativo, conformidade, código, design e assim por diante

#### Case
A Netflix decidiu migrar para a nuvem quando sofreu uma grande corrupção de banco de dados por três dias em 2008. A Netflix optou por reprojetar toda a sua tecnologia e mudar fundamentalmente a maneira como opera, com a AWS como provedora de nuvem. Alta confiabilidade, escalabilidade horizontal e sistemas distribuídos na nuvem foram fundamentais para seu sucesso. A refatoração completa levou anos, mas provou ser a melhor abordagem para eles. Hoje, tem 8 vezes mais membros do que em 2008 e é uma plataforma OTT global com presença em 130 países.

## Praticas recomendadas em uma migração

![Praticas Recomendadas](images/praticas_recomendandas.png "Praticas recomendadas")

### Conheça seu portifólio de TI
Para que sua estratégia de migração sejá acertiva, é muito importante conhecer completamente o portifólio de TI. Essa prática permite que você analise mais profundamente o ambiente existente, avaliando cada ativo em termos de custo, desempenho, tamanho, complexidade e dependências internas. Essa avaliação pode ser usada para desenvolver um caso de negócios e métricas de KPI para cada ativo para avaliar seu desempenho durante e após a migração. 

Existem ferramentas que podem auxiliar nesse processo um exemploe é o AWS Discovery Service.

### Projete sua estratégia
É muito importante que o time estejá bem ambientado com as estratégias de migração existentes e qual se aplica melhor ao cenário que nos econtramos. Tudo o que precisamos é começar pequeno com workloads menos criticos e seguir em frente com confiança. Considere as metas de negócios e os aspectos técnicos ao desenvolver seu plano de migração.

### Se impulsione com parceiros
Em alguns cenários o apóio de um parceiro pode auxiliar bastente na jornada de migração. É importante que o parceiro não tenha apenas o conhecimento técnico, mas também um portfólio diversificado e experiência. Esse parceiro deve ter compreensão profunda de todos os aspectos, como custo, ferramentas, conformidade, segurança, governança e requisitos de habilidades em nuvem.

### Prepare seu time e ambiente de TI
O treinamento é fundamental para capacitar sua equipe a se adaptar melhor às novas formas e facilitar uma migração mais tranquila. Para trazer mais clareza e transparência, você pode estabelecer um conjunto claro de funções, responsabilidades e protocolos para relatórios e gerenciamento. Também é importante preparar o ambiente de TI existente otimizando a conexão de rede, organizando recursos, gerenciando o acesso e implementando um sistema de conformidade, governança e segurança.

### Aproveite ferramentas de automação e serviços gerenciados
Uma das maneiras mais eficazes de migrar com eficiência para a nuvem é identificar padrões repetitivos. Você pode automatizá-los usando ferramentas para automação e serviços gerenciados que temos disponíveis. A automação acelera o processo, libera sua equipe para se concentrar em outras tarefas vitais e reduz o tempo de inatividade.

### Acompanhe e monitore
Lembre-se dos KPIs que você definiu no início? É hora de colocá-los em uso para medir e monitorar o processo. Essas métricas facilitam a identificação e a correção de problemas de desempenho durante a migração. As resoluções de problemas oportunas são essenciais para uma migração para a nuvem bem sucedida . Você também pode esperar uma nova estratégia com base nos eventos que se desenrolam. Não se esqueça de documentar o processo para referência futura.

### Teste e valide
Para testar e validar com precisão o sucesso da migração, use as mesmas metas de negócios e KPIs de aplicativos individuais definidos durante o planejamento. Ele destaca o escopo para otimização. Por exemplo, você pode ver a necessidade de refatorar um aplicativo inicialmente rehospedado na nuvem. Como as cargas de trabalho na nuvem evoluem continuamente, pode ser necessário melhorar a base.